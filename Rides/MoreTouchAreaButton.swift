//
//  MoreTouchAreaButton.swift
//  Oleico
//
//  Created by Moy Hdez on 01/12/16.
//  Copyright © 2016 Moy Hdez. All rights reserved.
//

import UIKit

class MoreTouchAreaButton: UIButton {
    
    override public func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let relativeFrame = self.bounds
        let hitTestEdgeInsets = UIEdgeInsetsMake(-44, -44, -44, -44)
        let hitFrame = UIEdgeInsetsInsetRect(relativeFrame, hitTestEdgeInsets)
        return hitFrame.contains(point)
    }
}
