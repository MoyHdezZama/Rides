//
//  APIConection.swift
//  Oleico
//
//  Created by Moy Hdez on 01/12/16.
//  Copyright © 2016 Moy Hdez. All rights reserved.
//

import UIKit

import UIKit
import Alamofire

class APIConection: NSObject {
    
    static let sharedInstance = APIConection()
    
    private override init() {
        super.init()
    }
    
    public enum requestURL: String{
        case addMamada = "AddMamdasToUser"
        case deleteUSer = "deleteUser"
        case getUSers = "getUsers"
        case addUser = "AddUser"
    }
    
    let baseURL: String = "http://schoolapphost.hol.es/api/"
    
    
    func getJson(requestURL: requestURL, parameters: Parameters?, method: Alamofire.HTTPMethod, vc: UIViewController?, completion: @escaping (Bool, Dictionary<String, AnyObject>?)->Void){
        
        var message: String = "Por favor revisa tu conexión a internet"
        
        let url = baseURL+requestURL.rawValue
        print(url)
        print(parameters ?? "")
        
        if vc != nil{
            LilithProgressHUD.show(vc?.view)
        }
        
        Alamofire.request(url, method: method, parameters: parameters).responseJSON { (response) in
            
            if vc != nil {
                LilithProgressHUD.hide(vc?.view)
            }
        
            if response.result.isSuccess {
                if let json = response.result.value as? [String:AnyObject]{
//                    print("JSON: \(json)")
                    print("\(json["code"] as! Int)")
                    if (json["code"] as? Int) == 200{
                        completion(true,(json["data"] as? Dictionary<String, AnyObject>))
                        return
                    }
                    message = json["message"] as! String
                }
            }
            
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            vc?.present(alert, animated: true, completion: nil)
            
            completion(false, nil)
            return
        }
        
    }
    
}
