//
//  Dude.swift
//  Rides
//
//  Created by Moy Hdez on 12/01/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class Dude: NSObject {
    var id: Int!
    var pic: String!
    var count: Int!
    
    override init() {
    }
    
    init(id: Int, pic: String, count: Int){
        self.id = id
        self.pic = pic
        self.count = count
    }
    
    init(data: Dictionary<String,Any>){
        self.id = Int(data["id"] as! String)
        self.pic = data["photo"] as! String
        self.count = Int(data["mamads"] as! String)
    }
}
