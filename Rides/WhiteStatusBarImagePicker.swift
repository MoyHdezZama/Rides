//
//  WhiteStatusBarImagePicker.swift
//  Rides
//
//  Created by Moy Hdez on 12/01/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class WhiteStatusBarImagePicker: UIImagePickerController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.white
        ]
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
}

