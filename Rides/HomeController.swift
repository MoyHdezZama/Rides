//
//  ViewController.swift
//  Rides
//
//  Created by Moy Hdez on 12/01/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import LocalAuthentication
import Alamofire

class HomeController: UIViewController {
    @IBOutlet weak var tableViewDudes: UITableView!
    var dudes: [Dude]! = []
    
    let imagePicker = WhiteStatusBarImagePicker()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    enum ThingsToDo {
        case deleteUser
        case addMamada
        case removeMamada
        case removeRide
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        
        tableViewDudes.addSubview(self.refreshControl)
        tableViewDudes.delegate = self
        tableViewDudes.dataSource = self
        
        getDudes(vc: self)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addDude(_ sender: Any) {
        showChoosePhoto()
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        getDudes(vc: nil) {
            refreshControl.endRefreshing()
        }
    }
    
    func showChoosePhoto(){
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Tomar foto", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File Deleted")
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.cameraCaptureMode = .photo
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.imagePicker.mediaTypes = ["public.image"]
                self.present(self.imagePicker,animated: true,completion: nil)
            } else {
                let alert = UIAlertController(title: "Cámara no disponible", message: "Parece que tu dispositivo no tiene cámara", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        })
        
        let saveAction = UIAlertAction(title: "Elegir de carrete", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("File Saved")
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = ["public.image"]
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func getDudes(vc: UIViewController?, completion:(() -> ())? = nil){
        APIConection.sharedInstance.getJson(requestURL: .getUSers, parameters: Parameters(), method: .get, vc: vc) { (success, data) in
            if success{
                self.dudes = []
                for user in data?["usuarios"] as! [Dictionary<String,Any>]{
                    self.dudes.append(Dude(data: user))
                }
                self.tableViewDudes.reloadData()
                completion?()
            }
        }
    }
}

extension HomeController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellVC", for: indexPath) as! CellVC
        
        let dude = dudes[indexPath.row]
        let ridesCount: Int = Int(floor(Double(dude.count / 10)))
        let count = dude.count - (ridesCount * 10)
        if ridesCount == 0{
            cell.ridesLbl.isHidden = true
        }else{
            cell.ridesLbl.isHidden = false
        }
        
        cell.countLbl.text = "\(count)"
        cell.ridesLbl.text = "Rides: \(ridesCount)"
        
        let dataDecoded:NSData = NSData(base64Encoded: dude.pic, options: NSData.Base64DecodingOptions(rawValue: 0))!
        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
        
        cell.profilePictureIV.image = decodedimage
        
        cell.plusBtn.tag = indexPath.row
        cell.plusBtn.addTarget(self, action: #selector(self.add(sender:)), for: .touchUpInside)
        cell.minusBtn.tag = indexPath.row
        cell.minusBtn.addTarget(self, action: #selector(self.minus(sender:)), for: .touchUpInside)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.removeRide(sender:)))
        cell.contentViewCell.tag = indexPath.row
        cell.contentViewCell.addGestureRecognizer(longPress)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dudes.count
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "      ") { action, index in
            print("Cancel button tapped")
            self.authenticateUser(option: .deleteUser, indexPath: indexPath)
        }
        
        delete.backgroundColor = UIColor(patternImage: UIImage.fontAwesomeIcon(name: .trash, textColor: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1), size: CGSize(width: 50, height: 130)))
        
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func add(sender: UIButton){
        let row: Int = sender.tag
        authenticateUser(option: .addMamada, indexPath: IndexPath(row: row, section: 0))
    }
    
    func removeRide(sender: AnyObject){
        if sender.state == UIGestureRecognizerState.began{
            print("long press")
            let row: Int = (sender as! UILongPressGestureRecognizer).view!.tag
            
            if(dudes[row].count >= 10){
                authenticateUser(option: .removeRide, indexPath: IndexPath(row: row, section: 0))
            }
        }
    }
    
    func minus(sender: UIButton){
        let row: Int = sender.tag
        if(dudes[row].count > 0){
            authenticateUser(option: .removeMamada, indexPath: IndexPath(row: row, section: 0))
        }
    }
    
    func addMamada(indexPath: IndexPath, numOfMamadas: Int){
        let parameters: Parameters = [
            "id":self.dudes[indexPath.row].id,
            "numOfMamds":numOfMamadas
        ]
        
        APIConection.sharedInstance.getJson(requestURL: .addMamada, parameters: parameters, method: .post, vc: self) { (success, data) in
            if success {
                self.dudes[indexPath.row].count = self.dudes[indexPath.row].count + numOfMamadas
                self.tableViewDudes.reloadRows(at: [indexPath], with: .fade)
            }
        }
    }
    
    func deleteUser(_ indexPath: IndexPath){
        let parameters: Parameters = [
            "id":self.dudes[indexPath.row].id
        ]
        
        APIConection.sharedInstance.getJson(requestURL: .deleteUSer, parameters: parameters, method: .post, vc: self) { (success, data) in
            if success {
                self.dudes.remove(at: indexPath.row)
                self.tableViewDudes.reloadData()
            }
        }
    }
    
    func addUser(_ dude: Dude){
        let parameters: Parameters = [
            "photo":dude.pic
        ]
        
        APIConection.sharedInstance.getJson(requestURL: .addUser, parameters: parameters, method: .post, vc: self) { (success, data) in
            if success {
                self.getDudes(vc: self)
            }
        }
    }
    
    func authenticateUser(option: ThingsToDo, indexPath: IndexPath){
        let context = LAContext()

        var error: NSError?
        var reasonString = ""
        
        switch option{
            case ThingsToDo.deleteUser:
                reasonString = "¿Quieres eliminar a este dude?"
            case ThingsToDo.addMamada:
                reasonString = "¿Quieres agregarle una mamada a este dude?"
            case ThingsToDo.removeMamada:
                reasonString = "¿Quieres eliminarle una mamada a este dude?"
            case ThingsToDo.removeRide:
                reasonString = "¿Quieres eliminarle un ride a este dude?"
        }
        
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: { (success, error) in
                DispatchQueue.main.async(execute: {
                    if success {
                        switch option{
                        case ThingsToDo.deleteUser:
                            self.deleteUser(indexPath)
                        case ThingsToDo.addMamada:
                            self.addMamada(indexPath: indexPath, numOfMamadas: 1)
                        case ThingsToDo.removeMamada:
                            self.addMamada(indexPath: indexPath, numOfMamadas: -1)
                        case ThingsToDo.removeRide:
                            self.addMamada(indexPath: indexPath, numOfMamadas: -10)
                        }
                        self.tableViewDudes.reloadData()
                    }
                })
            })
        }
    }
}

extension HomeController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imageToBase64(imageToBase64: UIImage) -> String{
        var imageBase64: String = ""
        let imageData = UIImageJPEGRepresentation(imageToBase64, 0.6)!
        imageBase64 = "\(imageData.base64EncodedString(options: .init(rawValue: 0)))"
        
        return imageBase64
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func resizeImage(image: UIImage, newHeight: CGFloat) -> UIImage {
        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        let chosenImage = info[UIImagePickerControllerOriginalImage]
        
        if let image = chosenImage as? UIImage {
            var dude: Dude!
            if image.size.width > image.size.height{
                if image.size.width > 1000{
                    dude = Dude.init(id: 0, pic: imageToBase64(imageToBase64: resizeImage(image: image, newWidth: 1000)), count: 0)
                }else{
                    dude = Dude.init(id: 0, pic: imageToBase64(imageToBase64: image), count: 0)
                }
            }else{
                if image.size.height > 1000{
                    dude = Dude.init(id: 0, pic: imageToBase64(imageToBase64: resizeImage(image: image, newWidth: 1000)), count: 0)
                }else{
                    dude = Dude.init(id: 0, pic: imageToBase64(imageToBase64: image), count: 0)
                }
            }
            self.addUser(dude)
        }
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

