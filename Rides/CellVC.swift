//
//  CellVC.swift
//  Rides
//
//  Created by Moy Hdez on 12/01/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit


class CellVC: UITableViewCell {
    @IBOutlet weak var profilePictureIV: UIImageView!
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var contentViewCell: UIView!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var ridesLbl: UILabel!
    
    override func awakeFromNib() {
        gradientView.startColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        gradientView.endColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.5)
        
        contentViewCell.layer.cornerRadius = 10
        
        profilePictureIV.layer.cornerRadius = profilePictureIV.frame.height/2
        gradientView.layer.cornerRadius = gradientView.frame.height / 2
    }
}
